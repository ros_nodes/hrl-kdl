hrl-kdl
=======

This repo is an updated fork of the https://github.com/gt-ros-pkg/hrl-kdl 
Added features:
 - gravity vector extraction
 - handling the joint torque limits

Kinematics and geometry utilities for KDL

See documentation here: http://wiki.ros.org/hrl-kdl
